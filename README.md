# container_row_column_alignment

This repo is about designing a layout using Containers in Flutter.
It aligns the view using both Row and Column.

 ![layout](images/layout.png)
